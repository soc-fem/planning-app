
(function () {
  const theWork = document.getElementById("thework");

  const sections = [];

  const scrubText = text => text.replace(/\n/g, " ").replace(/\s{2,}/g, " ");
  
  for (let element of theWork.children) {
    const { tagName, textContent } = element;
    sections.push({
      tagName,
      textContent: scrubText(textContent)
    })
  }

  const isHeader = tagName => /h\d/i.test(tagName);
  const isSectionContent = tagName => ["p", "table", "ul"].includes(tagName.toLowerCase());

  const getWordCount = sectionContents => sectionContents.join(" ").split(" ").filter(Boolean).length

  const parsedSections = sections.reduce(
    (accum, section, index, arr) => {
      let { sections, currentHeader, currentHeaderTag, currentSectionContents } = accum;
      if (isHeader(section.tagName)) {
        if (currentHeader.length > 0) {
          sections.push({
            title: currentHeader,
            sectionHeaderTag: currentHeaderTag,
            wordCount: getWordCount(currentSectionContents),
          });
        }
        currentHeader = section.textContent;
        currentHeaderTag = section.tagName;
        currentSectionContents = [];
      } else if (isSectionContent(section.tagName)) {
        currentSectionContents.push(section.textContent);
      }
      if (index === arr.length - 1) {
        sections.push({
          title: currentHeader,
          sectionHeaderTag: section.tagName,
          wordCount: getWordCount(currentSectionContents),
        });
      }
      return { sections, currentHeader, currentHeaderTag, currentSectionContents }
    },
    {
      currentHeader: "",
      currentHeaderTag: "",
      currentSectionContents: [],
      sections: [],
    }
  );

  const encodedData = encodeURIComponent(JSON.stringify(parsedSections, null, 2));
  const dataStr = `data:text/json;charset=utf-8,${encodedData}`;
  const dlAnchor = document.createElement("a");
  dlAnchor.setAttribute("href", dataStr);
  dlAnchor.setAttribute("download", "ocac-word-count.json");
  dlAnchor.click();
})();
