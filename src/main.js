


const app = Vue.createApp({
  data() {
    return {
      message: "Hello"
    }
  },
  template: /*html*/`
    <div>
      {{message}}
    </div>
  `
});

app.mount("#app");
